<?php

/**
 * pokedex functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package pokedex
 */

if ( ! defined( '_S_VERSION' ) ) {
    // Replace the version number of the theme on each release.
    define( '_S_VERSION', '1.0.0' );
}

function pokedex_setup() {

    load_theme_textdomain( 'pokedex', get_template_directory() . '/languages' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    register_nav_menus(
        array(
            'menu-1' => esc_html__( 'Primary', 'pokedex' ),
        )
    );
}
add_action( 'after_setup_theme', 'pokedex_setup' );

function pokedex_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'pokedex_content_width', 640 );
}
add_action( 'after_setup_theme', 'pokedex_content_width', 0 );

function pokedex_widgets_init() {
    register_sidebar(
        array(
            'name'          => esc_html__( 'Sidebar', 'pokedex' ),
            'id'            => 'sidebar-1',
            'description'   => esc_html__( 'Add widgets here.', 'pokedex' ),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );
}
add_action( 'widgets_init', 'pokedex_widgets_init' );

function pokedex_scripts() {
    wp_enqueue_style( 'pokedex-style', get_stylesheet_uri(), array(), _S_VERSION );
}
add_action( 'wp_enqueue_scripts', 'pokedex_scripts' );
